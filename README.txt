Introduction
============

fourdigits.portlet.twitter is a twitter portlet for Plone.
It contains some nice features such as:
- show tweets of a certain twitter user(name)
- search on multiple hastags or strings (e.g. show the tweets of #plone and #zope in one portlet)
- combine searched tweets and tweets based on username
- filter tweets on curse words
- configurable number of items to display based on the username
- configurable number of items to display based on the search(es)
- filter tweets by language (multiple supported, such as nl and en)
- user pictures from twitter
- user info from twitter
- multilanguage support

- Added support for clientside rendering using the libs from http://tweet.seaofclouds.com/
  You can turn this on in the portlet settings.

Hope you like the product!
heavelly based on collective.twitterportlet and a modified version of python-twitter
thanks guys!
